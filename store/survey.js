const prepareNewSurvay = () => {
    return {
        id: null,
        parameters: [],
        questions: [],
        logic: [],
        conditions: [],
        respondents: [],
    }
}


const prepareNewConditin = (type) => {
    return {
        id: null,
        type: type,
        values: []
    }
}


export const state = () => ({
    surveyList: [],
    currentSurvey: null,
    maxId: 0,
})

export const getters = () => ({
    conditions (state) {
        if( state.currentSurvey && state.currentSurvey.conditions) {
            return state.currentSurvey.conditions;
        }
        return [];
    },
    addCondition(state, payload) {
        state.currentSurvey.conditions.push({id: ++state.maxId})
    }
});

export const mutations = {
    setSurveysList (state, surveys) {
        state.surveyList = surveys ? surveys : [];
        console.log(`--- Mutation записать список опросов в стор` );
    },
}

export const actions = {
    addCondition ({commit, dispatch, state}, payload) {
        if (!state.currentSurvey) {
            commit('saveNewSurvey', payload);
        } 
        commit('addCondition', payload);
    }

} 
