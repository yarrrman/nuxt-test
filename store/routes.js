export const state = () => ({
    routeTranslates: {
        survey: "Опросы",
        parameters: 'Параметры',
        questions: 'Вопросы',
        logic: 'Логика',
        conditions: 'Условия',
        respondents: 'Респонденты',
        users: "Пользователи",
        blacklist: "Черные списки",
        calls: "Колл-центры",
        help: "Справка",
    }
})